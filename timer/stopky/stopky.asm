.dseg                ; prepnuti do pameti dat
.org 0x100          

; rezervovani mista pro promenne
counterN: .byte 1	 ;----Pro dekasekundy
counterP: .byte 1	 ;----Pro sekundy => presny cas
minutesF: .byte 1    
minutesS: .byte 1    
secondsF: .byte 1
secondsS: .byte 1
decaSeconds: .byte 1


.cseg                ; prepnuti do pameti programu

.include "m169def.inc" ; -- definice pro nas typ procesoru

.org 0x1000
.include "print.inc"   ; -- podprogramy pro praci s displejem

; Zacatek programu - po resetu
.org 0
jmp	start


.org 0xA             ; Preruseni
jmp interrupt


.org 0x100
start:
	cli

	;Inicializace zasobniku
	ldi r16, 0xFF
	out SPL, r16
	ldi r16, 0x04
	out SPH, r16

	;Inicializace komponent
	call init_disp
	call init_int
	call init_joy
	call reset_vars

main:
	;Main Program Loop
	call read_joy			;---Vstup z joysticku
	call display_change		;---Zobraz zmenu na display
	rjmp main


display_change:
	ldi r18, 48				;---48 ASCII znak == '0'
	
	lds r16, decaSeconds	;---Zobraz dekasekundy
	add r16, r18
	ldi r17, 7
	call show_char

	lds r16, secondsS		;---Zobraz jednotky sekund
	add r16, r18
	ldi r17, 6
	call show_char

	lds r16, secondsF		;---Zobraz desitky sekund
	add r16, r18
	ldi r17, 5
	call show_char

	lds r16, minutesS		;---Zobraz jednotky minut
	add r16, r18
	ldi r17, 3
	call show_char

	lds r16, minutesF		;---Zobraz desitky sekund
	add r16, r18
	ldi r17, 2
	call show_char

ret




; ----------------------- OVLADANI JOYSTICKU --------------------------------------
read_joy:
	in r16, PINB	;Nacti vstup joysticku (1)
	
	ldi r18, 255	;Pockej
 wait:
	dec r18
	brne wait	

	in r17, PINB	;Nacti vstup znovu (2)

	andi r16, 0b11010000
	andi r17, 0b11010000

	cp r16, r17
	brne read_joy	;Pokud se vstupy z joysticku 1 a 2 se lisi -> nacti znovu

	sbrs r16, 4		;Enter -> Vynuluj stopky
	call reset_vars
	
	sbrs r16, 6		;Nahoru	-> Start
	sei			

	sbrs r16, 7		;Dolu -> Stop
	cli


ret




; ----------------------- PRERUSENI -----------------------------------------------

interrupt:
	cli ;Zrus preruseni
	
	;Nahraj vse do zasobniku
	push r16
	in r16, SREG
	push r16
	push r17
	push r18
	push r19
	push r20
	push r21
	push r22


	;Nahraj cas z pameti do registru
	lds r16, counterN
	lds r17, minutesF
	lds r18, minutesS
	lds r19, secondsF
	lds r20, secondsS
	lds r21, decaSeconds
	lds r22, counterP		

	inc r22
	inc r16

	cpi r22, 128 ;----Sekunda se pricte jednou za 128 preruseni
	brne deka
	ldi r22, 0
	jmp cntP
deka:
	cpi r16, 13	 ;----Dekasekunda se pricte jednou za 13 preruseni
	brne save

cntP:
	;Zvysuj ostatni jednotky (desitky sekund, jednotky minut a desitky minut)
	ldi r16, 0
	

	inc r21
	cpi r21, 10
	brne save
	ldi r21, 0
	
	inc r20
	cpi r20, 10
	brne save
	ldi r20, 0

	inc r19
	cpi r19, 6
	brne save
	ldi r19, 0

	
	inc r18
	cpi r18, 10
	brne save
	ldi r18, 0

	inc r17
	cpi r17, 6
	brne save
	ldi r17, 0
 save:
 	;Nahraj nove hodnoty do pameti
	sts counterN, r16
	sts minutesF, r17
	sts minutesS, r18
	sts secondsF, r19
	sts secondsS, r20
	sts decaSeconds, r21
	sts counterP, r22

	;Vyzvedni puvodni hodnoty ze zasobniku
	pop r22
	pop r21
	pop r20
	pop r19
	pop r18
	pop r17
	pop r16
	out SREG, r16
	pop r16



reti ;Navrat s obnovenim preruseni




; ----------------------- INICIALIZACE KOMPONENT ----------------------------------
reset_vars:
	ldi r21, 0
	brid no_int
	 ldi r21, 1
	 cli
 no_int:	
	ldi r22, 0
	sts counterN, r22
	sts minutesF, r22
	sts minutesS, r22
	sts secondsF, r22
	sts secondsS, r22
	sts decaSeconds, r22
	sts counterP, r22

	sbrc r21, 0
	sei
ret


init_joy:
    ; nastaveni portu E (smer vlevo a vpravo)
    in r17, DDRE         ; 1
    andi r17, 0b11110011
    in r16, PORTE
    ori r16, 0b00001100
    out DDRE, r17
    out PORTE, r16
    ldi r16, 0b00000000
    sts DIDR1, r16

    ; nastaveni portu B (smer dolu, nahoru a enter)
    in r17, DDRB
    andi r17, 0b00101111
    in r16, PORTB
    ori r16, 0b11010000
    out DDRB, r17
    out PORTB, r16
ret

init_int:
    ldi r16, 0b00001000
    sts ASSR, r16    ; vyber hodin od externiho krystaloveho oscilátoru 32768 Hz
    ldi r16, 0b00000001
    sts TIMSK2, r16  ; povoleni preruseni od casovace 2
    ldi r16, 1
    sts TCCR2A, r16  ; nastaveni deliciho pomeru 1
    clr r16
    out EIMSK, r16   ; zakazani preruseni od joysticku
ret


