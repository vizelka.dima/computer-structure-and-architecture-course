# Elektronické stopky

Pro přípravek AVR Butterfly implementujte elektronické stopky. Na displeji se v desítkové soustavě zobrazuje informace o čase ve tvaru MM:SS:DD, kde MM jsou minuty, SS jsou sekundy, DD jsou setiny sekund. Setiny sekund nemusí být přesné (protože ani nemohou), můžete se i omezit na zobrazování pouze desetin sekundy.

## Požadavky na úlohu
Stopky se ovládají joystickem. 3 různé polohy joysticku mají význam START, STOP a RESET.

Důležitá je přesnost stopek. Mikroprocesor je taktován kmitočtem 2 MHz. Ovšem není možné přesně měřit čas pomocí počítání taktů CPU (pomocí čekacích smyček). Pro získání plného počtu bodů je vyžadováno použití časovače a přerušení.

## Bonusy navíc
- Obsluha joysticku pomocí přerušení,
- funkce mezičasu:
	- po aktivaci poběží stopky dál, na displeji zůstane zobrazen mezičas,
	- po další aktivaci se opět zobrazí aktuální čas (a tak dál).

