# Běžící text
Napište program, který na displeji přípravku zobrazí běžící text. Text bude uložen v paměti programu. Jako příklad pro představu jsou elektronické informativní tabule v metru, kde se zobrazují příští stanice a nebo běžící text „Vystupujte vpravo, ve směru jízdy“.

Program musí být schopen zobrazit jakýkoliv text bez diakritiky uložený v paměti programu (nezáleží na délce textu, jediné omezení je velikost paměti programu). Text bude uložen v paměti programu pomocí direktivy .db. Tento program by měl používat volání podprogramů (showchar se nepočítá).

Svůj text si upravte na svůj vlastní a zajistěte, aby se pohyboval vhodnou rychlostí. Pokud na přípravku nápis nepůjde přečíst (bude jezdit moc rychle) nebo bude nápis jezdit moc pomalu, upravte čekací smyčku.

Čekací smyčka je jednoduchý nástroj, jak v mikroprocesoru měřit čas. Jedná se o základní metodu busy waiting, což nám však pro začátek postačí. Vhodným řešením je použití smyček/cyklů/loopů, kde se neprovádí žádný užitečný výpočet, ale samotné instrukce pro skoky a snižování čítače stojí takty procesoru. Čekací smyčka je jedním z vhodných kandidátů pro zavolání podprogramu.

Pro vyčítání dat z paměti programu se využívá instrukce LPM. Při používání instrukce je nutné mít správně nastaven šestnáctibitový registr např. Z skládající se z registrů R30(ZL) a R31(ZH). Mějte na paměti, že adresace se provádí v bytech a ne ve slovech (paměť programu je 16bitová), a proto se adresa ve slovech musí násobit dvěma.

Směr zobrazování nápisu bude volitelný pomocí joysticku (doleva/doprava).
