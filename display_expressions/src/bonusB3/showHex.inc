.include "print.inc"
show_hex:
	push r22
	push r19
	push r18
	push r17
	push r16
	
	mov r19, r16

	andi r16, 0x0F
numOrChar:
	cpi r16, 10
	brlo num 
	ldi r18, 55
	jmp show
num:
	ldi r18, 48
show:
	add r16, r18
	call show_char
	dec r17
	cp r17, r22
	breq end
second:
	mov r16, r19
	lsr r16
	lsr r16
	lsr r16
	lsr r16
	jmp numOrChar
end:
	pop r16
	pop r17
	pop r18
	pop r19
	pop r22
	ret
