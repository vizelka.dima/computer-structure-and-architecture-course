.include "m169def.inc"
.org 0x1000
.include "showHex.inc" ; upravena uloha 2 (zobrazeni hex cisla)

.org 0
jmp start

.org 0x100

; V pripade nepresnosti (->zaokrouhleni pri deleni) se na display zobrazi 'R'
; zpusob zobrazen => BIG-ENDIAN
start:
	ldi r16, 0xFF
	out SPL, r16
	ldi r16, 0x04
	out SPH, r16

	call init_disp	

	ldi r16, 5     ; <-----------R16 hodnota
	ldi r17, 10    ; <-----------R17 hodnota
	ldi r18, 57    ; <-----------R18 hodnota

	;Nasobeni 4*R16
	ldi r19, 4
	muls r16, r19

	mov r26, r1
	mov r27, r0

	;Nasobeni 3*R17
	ldi r19,3
	muls r17, r19

	;Pricteni obou vysledku
	add r27, r0
	adc r26, r1

	;odecteni R18	
	sub r27, r18
	sbci r26, 0
	
	;Deleni  
	ldi r29, 0 ; detektor nepresnosti
		asr r26
		ror r27
		brcc devide2
			inc r29
	
	devide2:
		asr r26
		ror r27
		brcc devide3
			inc r29
	devide3:
		asr r26
		ror r27
		brcc display
			inc r29

	; Pri nepresnosti bude r29 nenulove => zobrazi se 'R' => zaokrouhleni
display:
	mov r16, r26
	ldi r17, 3
	ldi r22, 1
	call show_hex

	mov r16, r27
	ldi r17, 5
	ldi r22, 3
	call show_hex
	
	cpi r29, 0 ; vypis 'R' pri nepresnosti
	breq finish
	ldi r16, 'R'
	ldi r17, 7
	call show_char 
finish:
	rjmp finish


	
