.include "m169def.inc"

.org 0
jmp start

.org 0x100


; V pripade nespravnosti(=>nestaci 8 bitu) => nastavi se V flag na 1 a skonci ........ v pripade nepresnosti(pri deleni) se pouze nastavi V falg na 1, ale dopocita do konce
; Vysledek v r20

start:
				; NASTAVENI HODNOT
	ldi r16, 29 ; <-----------R16
	ldi r17, 5	; <-----------R17
	ldi r18, 69	; <-----------R18

first:				; PRVNI CISLO R16
	lsl r16			; vynasob *2
	brcs fnegative	; bylo zaporne -> kontrola pro zaporne (fnegative)
			
	brmi fail		; else         -> kontrola pro kladne => vynasobenim kladneho nesmime dostat zaporne 
	lsl r16			
	brmi fail
	jmp second

fnegative:
	brpl fail
	lsl r16
	brpl fail

second:				
	mov r20, r16	; do vysledku zapis prvni vysledek (4*r16)
	
					; DRUHE CISLO R17

	mov r19, r17	; vytvor kopii
	
	lsl r17			; vynasob *2
	brcs snegative	
	brmi fail

	add r19, r17	; pricti kopii puvodniho cisla
	brmi fail
	
	add r20, r19	; pricti vysledek 3*R17 k soucasnemu vysledku
	brmi fail

	cp r20, r16		; prictenim kladneho cisla nesmim dostat mensi cislo
	brlt fail

	jmp third

snegative:
	brpl fail
	
	add r19, r17
	brpl fail

	add r20, r19

	cp r16, r20 	; prictenim zaporneho cisla nesmim dostat vetsi cislo
	brlt fail


third:				; TRETI CISLO R18
	mov r21, r20
	cpi r18, 0
	brmi tnegative

	sub r20, r18
	cp r21, r20
	brlt fail
	jmp devide

tnegative:
	add r20, r18
	cp r20, r21
	brlt fail

devide:
	asr r20
	brcc devide2 
	sev				; pokud jsme delili liche cislo => ztrata presnosti 
devide2:
	asr r20
	brcc devide3
	sev
devide3:
	asr r20
	brcc end
	sev
end:
	rjmp end

fail:
	sev			; V flag na 1
	jmp end
