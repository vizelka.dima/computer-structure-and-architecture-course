.include "m169def.inc"

; podprogramy pro praci s displejem
.org 0x1000
.include "print.inc"


.org 0
jmp start

.org 0x100


; V pripade nespravnosti => Na display se zobrazi "FAIL" ( vyraz vsak vzdy dopocita do konce )
; Vysledek v R20

start:
	;Inicializace zasobniku
	ldi r16, 0xFF
	out SPL, r16
	ldi r16, 0x04
	out SPH, r16

	; Inicializace displaye
	call init_disp	

				; NASTAVENI HODNOT
	ldi r16, 28 ; <-----------R16
	ldi r17, 10	; <-----------R17
	ldi r18, 58	; <-----------R18

	ldi r22, 1 	; detektor vypisu chyby

;-------------------- PRVNI CISLO R16 --------------------

	lsl r16			; R16 *= 2
	brvc op2
	call fail 
op2:
	lsl r16			; R16 *= 2
	brvc op3
	call fail

op3:
	mov r20, r16	; R20 = R16
	


;-------------------- DRUHE CISLO R17 --------------------

	mov r19, r17	; R19 = R17
	lsl r17			; R17 *= 2
	brvc op4
	call fail
op4:
	add r19, r17	; R19 += R17
	brvc op5
	call fail
op5:
	add r20, r19	; R20 += R19 
	brvc op6
	call fail
op6:
	

;-------------------- TRETI CISLO R18 --------------------	

	sub r20, r18	; R20 -= R18
	brvc op7
	call fail


;-------------------- DELENI -----------------------------

op7:
	asr r20			; R20 /= 2
	brcc devide2 
	call fail		; pokud jsme delili liche cislo => ztrata presnosti 
devide2:
	asr r20			; R20 /= 2
	brcc devide3
	call fail
devide3:
	asr r20			; R20 /= 2
	brcc end
	call fail

; END ----------
end:
	rjmp end

;-------------------- PODPROGRAM K VYPISU CHYBY ----------

fail:
	cpi r22, 1		; pokud se chyba jiz vyskytla, nepotrebujeme ji vypisovat znovu
	breq show
	ret
	
  show:
	push r16
	in r16, SREG		
	push r16
	push r17
	
	ldi r16, 'F'
	ldi r17, 2
	call show_char	

	ldi r16, 'A'
	ldi r17, 3
	call show_char	

	ldi r16, 'I'
	ldi r17, 4
	call show_char	

	ldi r16, 'L'
	ldi r17, 5
	call show_char	

	ldi r22, 0

	pop r17
	pop r16
	out SREG, r16
	pop r16
	ret

