.include "m169def.inc"

; podprogramy pro praci s displejem
.org 0x1000
; 
; podprogramy pro praci s displejem
.org 0x1000
.include "print.inc"

.org 0
jmp start

start:
	; Inicializace zasobniku
	ldi r16, 0xFF
	out SPL, r16
	ldi r16, 0x04
	out SPH, r16

	; Inicializace displaye
	call init_disp	


	ldi r16, 0xA0 	; <<<======== Cislo
	
	ldi r17, 3 		; pozice prave cislice
	mov r19, r16 	; temp 

	andi r16, 0x0F 	; zjisti hodnotu prave cislice

numOrChar:
	cpi r16, 10 
	brlo num		; mensi nez 10 -->> dec cislice
			 		; else         -->> hex cislice 
	ldi r18, 55 	; 55 == (pozice 'A' je 65) MINUS (hodnota 'A' 10)
	jmp show

num:
	ldi r18, 48 	; pozice cislice '0'
show:
	add r16, r18
	call show_char	; Zobraz jednu cislici

	dec r17 		; po zobrazeni prave cislice, zmens pozici pro levou
	cpi r17, 1
	breq end		; pokud leva jiz zobrazena -->> konec
					; else 					   -->> zjisti hodnotu leve cislice
	mov r16, r19
	lsr r16
	lsr r16
	lsr r16
	lsr r16

	jmp numOrChar
end:
	rjmp end
